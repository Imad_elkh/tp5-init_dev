#Exercice 1

def forth_val(liste,valeur):
    """mystere renvoie la position de la 4eme rencontre avec valeur

    Args:
        liste (list): Liste de valeurs à analyser
        valeur (int): Valeur à retrouver dans la liste

    Returns:
        [int]: Position du 4eme valeur rencontré
    """
    forth=0 #Au début de chaque boucle yyy vaut le nombre d'element recontré égals à valeur
    for i in range(len(liste)):
        if liste[i]==valeur:
            forth+=1
            if forth>3:
                return i
    return None

def test_forth_val():
    assert forth_val([12,5,20,48,20,418,185,20,20,87],20) == 8
    assert forth_val([12,5,20,48,20,418,185,0,20,87],20) == None
    assert forth_val([],20) == None


#Exercice 2.1
def indice_int(liste):
    chiffre="0123456789"
    for i in range(len(liste)):
        if liste[i] in chiffre:
            return i
    return None    

def test_indice_int():
    assert indice_int("on est le 30/09/2021") == 10
    assert indice_int("on est le") == None
    assert indice_int("") == None

#Exercice 2.2
# # --------------------------------------
# Exemple de villes avec leur population
# --------------------------------------
liste_villes=["Blois","Bourges","Chartres","Châteauroux","Dreux","Joué-lès-Tours","Olivet","Orléans","Tours","Vierzon"]
population=[45871, 64668,  38426, 43442, 30664, 38250, 22168, 116238, 136463,  25725]

def return_pop(ville):
    for i in range(len(liste_villes)):
        if liste_villes[i] == ville:
            return population[i]
    return None

def test_return_pop():
    assert return_pop("Blois")==45871
    assert return_pop("")==None
    assert return_pop("Vierzon")==25725
    assert return_pop("Orleans")==None

#Exercice 3.1
def is_tri_liste(liste):
    """Verifie si une liste est bien triée

    Args:
        liste (list): Liste à verifier

    Returns:
        bool: Liste triée ou pas
    """    
    for i in range(1,len(liste)):
        if liste[i]>liste[i-1]:
            return True
    return False

#Exercice 3.2[summary]
def seuil(liste,seuil):
    """Verifie si la somme des éléments d'une liste dépasse un seuil

    Args:
        liste (list): Liste d'éléments
        seuil (int): Seuil

    Returns:
        Bool: True si le seuil est dépassé
    """    
    som=0
    for i in range(len(liste)):
        som+=liste[i]
    return som>seuil

def test_seuil():
    assert (seuil([1,4,1,2,3,4],6))==True
    assert (seuil([1,2,3],7))==False
    assert (seuil([],3))==False

#Exercice 3.3

def verif_mail(chaine):
    """Verifie si une chaine correspond à une adresse mail

    Args:
        chaine (str)): Chaine a verifier

    Returns:
        bool: Retourne True si la chaine correspond a une adresse mail
    """    
    arobase=False
    res=False
    nb_arobase=0
    nb_point=0
    for i in range(len(chaine)):
        elem=chaine[i]
        if elem==" ":
            return False
        elif elem=="." and arobase:
            res=True
            nb_point+=1
        elif elem=="@":
            arobase=True
            nb_arobase+=1
    if nb_arobase>1 or nb_point>1:
        res=False        
    elif chaine[-1]=="." or chaine[0]==".":
        res=False
    return res       

def test_verif_mail():
    assert(verif_mail("imad@gmail.com"))==True
    assert(verif_mail("im@ad@gmail.com"))==False
    assert(verif_mail("ima.d@gmailcom"))==False
    assert(verif_mail("imadgmail.com"))==False
    assert(verif_mail("imad @gmail.com"))==False
    assert(verif_mail("imad@gm.ail.com"))==False
    assert(verif_mail("m"))==False


