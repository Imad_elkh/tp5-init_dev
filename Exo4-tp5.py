#---------------------------------------
# Exemple de scores
#---------------------------------------
scores=[352100,325410,312785,220199,127853]
joueurs=['Batman','Robin','Batman','Joker','Batman']

#Ex 4.1

def record_joueur(nom):
    """Donne le records d'un joueur

    Args:
        nom (str): Nom du joueur

    Returns:
        int: Donne le meilleur score du joueur donné
    """    
    record=0
    if nom not in joueurs:
        return None
    for i in range(len(scores)):
        score=scores[i]
        if joueurs[i]==nom:
            if record<score:
                record=score
    return record

def test_record_joueur():
    assert(record_joueur("Batman"))==352100
    assert(record_joueur("Joker"))==220199
    assert(record_joueur(""))==None
    assert(record_joueur("Imad"))==None

#ex 4.2

def score_decroiss(score):
    """Verifie si une liste de score est triée dans l'ordre décroissant

    Args:
        score (list): Liste de scores

    Returns:
        bool: True si la liste est triée dans l'ordre décroissant
    """    
    if score==[]:
        return None
    for i in range(1,len(score)):
        if score[i]>score[i-1]:
            return False
    return True

def test_score_decroiss():
    assert(score_decroiss(scores))==True
    assert(score_decroiss([352100,325410,312785,220199,50000000]))==False
    assert(score_decroiss([]))==None

#ex 4.3

def nb_occ_joueur(nom):
    """Donne le nombre d'occurences d'un joueur dans une liste

    Args:
        nom (str): Nom à compter

    Returns:
        int: Donne le nombre d'occurence de nom dans joueurs
    """    
    occ=0
    if nom not in joueurs:
        return None
    for i in range(len(joueurs)):
        if joueurs[i]==nom:
            occ+=1
    return occ

def test_nb_occ_joueur():
    assert(nb_occ_joueur("Batman"))==3
    assert(nb_occ_joueur("Joker"))==1
    assert(nb_occ_joueur(""))==None
    assert(nb_occ_joueur("Imad"))==None

#ex 4.4

def premiere_app(nom):
    """Donne la position du plus grand score d'un joueur dans une liste triée

    Args:
        nom (str): Nom à chercher

    Returns:
        int : Position du meilleur score d'un joueur
    """    
    for i in range(len(joueurs)):
        if nom==joueurs[i]:
            return i+1
    return None

def test_premiere_app():
    assert(premiere_app("Batman"))==1
    assert(premiere_app("Joker"))==4
    assert(premiere_app(""))==None
    assert(premiere_app("Oui-oui"))==None

#ex 4.5

def 
